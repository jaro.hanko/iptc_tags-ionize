<?php

		DEFINE('IPTC_OBJECT_NAME', '005');
    DEFINE('IPTC_EDIT_STATUS', '007');
    DEFINE('IPTC_PRIORITY', '010');
    DEFINE('IPTC_CATEGORY', '015');
    DEFINE('IPTC_SUPPLEMENTAL_CATEGORY', '020');
    DEFINE('IPTC_FIXTURE_IDENTIFIER', '022');
    DEFINE('IPTC_KEYWORDS', '025');
    DEFINE('IPTC_RELEASE_DATE', '030');
    DEFINE('IPTC_RELEASE_TIME', '035');
    DEFINE('IPTC_SPECIAL_INSTRUCTIONS', '040');
    DEFINE('IPTC_REFERENCE_SERVICE', '045');
    DEFINE('IPTC_REFERENCE_DATE', '047');
    DEFINE('IPTC_REFERENCE_NUMBER', '050');
    DEFINE('IPTC_CREATED_DATE', '055');
    DEFINE('IPTC_CREATED_TIME', '060');
    DEFINE('IPTC_ORIGINATING_PROGRAM', '065');
    DEFINE('IPTC_PROGRAM_VERSION', '070');
    DEFINE('IPTC_OBJECT_CYCLE', '075');
    DEFINE('IPTC_BYLINE', '080');
    DEFINE('IPTC_BYLINE_TITLE', '085');
    DEFINE('IPTC_CITY', '090');
    DEFINE('IPTC_PROVINCE_STATE', '095');
    DEFINE('IPTC_COUNTRY_CODE', '100');
    DEFINE('IPTC_COUNTRY', '101');
    DEFINE('IPTC_ORIGINAL_TRANSMISSION_REFERENCE',     '103');
    DEFINE('IPTC_HEADLINE', '105');
    DEFINE('IPTC_CREDIT', '110');
    DEFINE('IPTC_SOURCE', '115');
    DEFINE('IPTC_COPYRIGHT_STRING', '116');
    DEFINE('IPTC_CAPTION', '120');
    DEFINE('IPTC_LOCAL_CAPTION', '121');
		
/**
 * Class to basic manupulation with IPTC tags
 */		
class Iptc {
	
	var $meta = array();
	var $hasmeta = false;
	var $file = false;

	/**
	 * 
	 * @param string $filename path to the file
	 */
	public function __construct($filename) {
		$size = getimagesize($filename, $info);
		$this->hasmeta = isset($info["APP13"]);
		if ($this->hasmeta)
			$this->meta = iptcparse($info["APP13"]);
		$this->file = $filename;
	}
	
	/**
	 * set new TAG to the image - without writing
	 * @param integer $tag
	 * @param string|array $data
	 */
	public function set($tag, $data) {
		if (is_array($data)) {

			foreach ($data as $i => $v) {
				$this->meta["2#$tag"][$i] = $v;
			}
			$this->hasmeta = true;
		} else {
			$this->meta["2#$tag"] = Array($data);
			$this->hasmeta = true;
		}
	}

	/**
	 * get concrete tag
	 * @param integer $tag
	 * @return boolean
	 */
	public function get($tag = FALSE) {
		if ($tag === FALSE)
			return $this->meta;
		if (!isset($this->meta["2#$tag"]))
			return FALSE;
		if (sizeof($this->meta["2#$tag"]) == 1)
			return reset($this->meta["2#$tag"]);
		return $this->meta["2#$tag"];
	}

	/**
	 * dump all stored tags
	 */
	public function dump() {
		print_r($this->meta);
	}

	/**
	 * 
	 * @return string
	 */
	public function binary() {
		$iptc_new = '';
		foreach (array_keys($this->meta) as $s) {
			$tag = str_replace("2#", "", $s);
			if (count($this->meta[$s]) > 1) {
				foreach ($this->meta[$s] as $row) {
					$iptc_new .= $this->iptc_maketag(2, $tag, $row);
				}
			} else {
				$iptc_new .= $this->iptc_maketag(2, $tag, $this->meta[$s][0]);
			}
		}
		return $iptc_new;
	}

	/**
	 * 
	 * @param type $rec
	 * @param type $dat
	 * @param type $val
	 * @return type
	 */
	public function iptc_maketag($rec, $dat, $val) {
		$len = strlen($val);
		if ($len < 0x8000) {
			return chr(0x1c) . chr($rec) . chr($dat) .
					chr($len >> 8) .
					chr($len & 0xff) .
					$val;
		} else {
			return chr(0x1c) . chr($rec) . chr($dat) .
					chr(0x80) . chr(0x04) .
					chr(($len >> 24) & 0xff) .
					chr(($len >> 16) & 0xff) .
					chr(($len >> 8 ) & 0xff) .
					chr(($len ) & 0xff) .
					$val;
		}
	}

	/**
	 * 
	 * @param string $newFileName
	 * @param boolean $removeOld
	 * @return boolean|Iptc
	 * @throws Exception
	 */
	public function write($newFileName = NULL, $removeOld = TRUE) {
		try {
			if (!function_exists('iptcembed'))
				throw new Exception("Function [iptcembed] dont exist ");

			$mode = 0;
			$content = iptcembed($this->binary(), $this->file, $mode);
			$filename = $this->file;

			if ($removeOld || is_null($newFileName))
				@unlink($filename);#delete if exists

			$filename = (is_null($filename)) ? $filename : $newFileName;
			$fp = fopen($filename, "wb");
			fwrite($fp, $content);
			fclose($fp);
			return TRUE;
		} catch (Exception $e) {
			trigger_error($e->getMessage(), E_ERROR);
			print_r($e->getMessage());
			return FALSE;
		}
	}
	
	/**
	 * 
	 * @param string $newFileName
	 * @param boolean $removeOld
	 * @return boolean|Iptc
	 * @throws Exception
	 */
	public function copyTo($dest_fullFilePath) {
		try {
			if (!function_exists('iptcembed'))
				throw new Exception("Function [iptcembed] dont exist ");

			$mode = 0;
			$content = iptcembed($this->binary(), $dest_fullFilePath, $mode);
			$filename = $dest_fullFilePath;

			$fp = fopen($filename, "wb");
			fwrite($fp, $content);
			fclose($fp);
			return TRUE;
		} catch (Exception $e) {
			trigger_error($e->getMessage(), E_ERROR);
			print_r($e->getMessage());
			return FALSE;
		}
	}
	
}